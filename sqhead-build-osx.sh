#!/bin/bash
set -e

# Note Live555 is downloaded from http://www.live555.com/liveMedia/public/
# using the live555-latest.tar.gz file.

rm -r osx || true
mkdir osx

./genMakefiles macosx
make

cp BasicUsageEnvironment/libBasicUsageEnvironment.a osx/
cp UsageEnvironment/libUsageEnvironment.a osx/
cp groupsock/libgroupsock.a osx/
cp liveMedia/libliveMedia.a osx/

make clean

rm -rf BasicUsageEnvironment/Makefile
rm -rf Makefile
rm -rf UsageEnvironment/Makefile
rm -rf groupsock/Makefile
rm -rf liveMedia/Makefile
rm -rf mediaServer/Makefile
rm -rf proxyServer/Makefile
rm -rf testProgs/Makefile