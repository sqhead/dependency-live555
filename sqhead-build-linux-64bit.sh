#!/bin/bash
set -e

# Note Live555 is downloaded from http://www.live555.com/liveMedia/public/
# using the live555-latest.tar.gz file.

rm -r linux-64bit || true
mkdir linux-64bit

./genMakefiles linux-64bit
make

cp BasicUsageEnvironment/libBasicUsageEnvironment.a linux-64bit/
cp UsageEnvironment/libUsageEnvironment.a linux-64bit/
cp groupsock/libgroupsock.a linux-64bit/
cp liveMedia/libliveMedia.a linux-64bit/

make clean

rm -rf BasicUsageEnvironment/Makefile
rm -rf Makefile
rm -rf UsageEnvironment/Makefile
rm -rf groupsock/Makefile
rm -rf liveMedia/Makefile
rm -rf mediaServer/Makefile
rm -rf proxyServer/Makefile
rm -rf testProgs/Makefile
