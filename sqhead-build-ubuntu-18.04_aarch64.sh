#!/bin/bash
set -e

# Note Live555 is downloaded from http://www.live555.com/liveMedia/public/
# using the live555-latest.tar.gz file.

rm -r ubuntu-18.04_aarch64 || true
mkdir ubuntu-18.04_aarch64

./genMakefiles linux
export CFLAGS="-fPIC"
export CXXFLAGS="-fPIC" 
make -j6

cp BasicUsageEnvironment/libBasicUsageEnvironment.a ubuntu-18.04_aarch64/
cp UsageEnvironment/libUsageEnvironment.a ubuntu-18.04_aarch64/
cp groupsock/libgroupsock.a ubuntu-18.04_aarch64/
cp liveMedia/libliveMedia.a ubuntu-18.04_aarch64/

make clean

rm -rf BasicUsageEnvironment/Makefile
rm -rf Makefile
rm -rf UsageEnvironment/Makefile
rm -rf groupsock/Makefile
rm -rf liveMedia/Makefile
rm -rf mediaServer/Makefile
rm -rf proxyServer/Makefile
rm -rf testProgs/Makefile
