#!/bin/bash
set -e

# Note Live555 is downloaded from http://www.live555.com/liveMedia/public/
# using the live555-latest.tar.gz file.

rm -r linux-32bit || true
mkdir linux-32bit

./genMakefiles linux
make

cp BasicUsageEnvironment/libBasicUsageEnvironment.a linux-32bit/
cp UsageEnvironment/libUsageEnvironment.a linux-32bit/
cp groupsock/libgroupsock.a linux-32bit/
cp liveMedia/libliveMedia.a linux-32bit/

make clean

rm -rf BasicUsageEnvironment/Makefile
rm -rf Makefile
rm -rf UsageEnvironment/Makefile
rm -rf groupsock/Makefile
rm -rf liveMedia/Makefile
rm -rf mediaServer/Makefile
rm -rf proxyServer/Makefile
rm -rf testProgs/Makefile
