#!/bin/bash
set -e

# Note Live555 is downloaded from http://www.live555.com/liveMedia/public/
# using the live555-latest.tar.gz file.

rm -r linux_arm || true
mkdir linux_arm

./genMakefiles linux 
make

cp BasicUsageEnvironment/libBasicUsageEnvironment.a linux_arm/
cp UsageEnvironment/libUsageEnvironment.a linux_arm/
cp groupsock/libgroupsock.a linux_arm/
cp liveMedia/libliveMedia.a linux_arm/

make clean

rm -rf BasicUsageEnvironment/Makefile
rm -rf Makefile
rm -rf UsageEnvironment/Makefile
rm -rf groupsock/Makefile
rm -rf liveMedia/Makefile
rm -rf mediaServer/Makefile
rm -rf proxyServer/Makefile
rm -rf testProgs/Makefile
